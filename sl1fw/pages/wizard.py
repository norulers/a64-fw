# This file is part of the SL1 firmware
# Copyright (C) 2014-2018 Futur3d - www.futur3d.net
# Copyright (C) 2018-2019 Prusa Research s.r.o. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

# TODO: Fix following pylint problems
# pylint: disable=no-else-return
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-return-statements
# pylint: disable=too-many-branches
# pylint: disable=too-many-statements

import re
from dataclasses import dataclass, asdict
from time import sleep

import distro
from prusaerrors.sl1.codes import Sl1Codes

from sl1fw import defines
from sl1fw.api.decorators import wrap_exception
from sl1fw.errors.errors import TowerAxisCheckFailed, ConfigException, get_exception_code
from sl1fw.functions.checks import tower_axis
from sl1fw.functions.system import shut_down
from sl1fw.functions.files import save_wizard_history
from sl1fw.configs.toml import TomlConfig
from sl1fw.pages import page
from sl1fw.pages.base import Page
from sl1fw.pages.wait import PageWait
from sl1fw.states.display import DisplayState
from sl1fw.hardware.tilt import TiltProfile


@dataclass(init=False)
class WizardData:
    # following values are for quality monitoring systems
    osVersion: str
    a64SerialNo: str
    mcSerialNo: str
    mcFwVersion: str
    mcBoardRev: str
    towerHeight: int
    tiltHeight: int
    uvPwm: int

    # following values are measured and saved in initial wizard
    # data in mV for 1/6, 1/2, 1/1 of max PWM for MC board
    wizardUvVoltageRow1: list
    # data in mV for 1/6, 1/2, 1/1 of max PWM for MC board
    wizardUvVoltageRow2: list
    # data in mV for 1/6, 1/2, 1/1 of max PWM for MC board
    wizardUvVoltageRow3: list
    # fans RPM when using default PWM
    wizardFanRpm: list
    # UV LED temperature at the beginning of test (should be close to ambient)
    wizardTempUvInit: float
    # UV LED temperature after warmup test
    wizardTempUvWarm: float
    # ambient sensor temperature
    wizardTempAmbient: float
    # A64 temperature
    wizardTempA64: float
    # measured fake resin volume in wizard (without resin with rotated platform)
    wizardResinVolume: int
    # tower axis sensitivity for homing
    towerSensitivity: int
#endclass


@page
class PageWizardBase(Page):
    def backButtonRelease(self):
        return PageWizardSkip.Name
    #enddef

    def _EXIT_(self):
        self.allOff()
        return "_EXIT_"
    #enddef


@page
class PageWizardInit(PageWizardBase):
    Name = "wizardinit"

    def __init__(self, display):
        super(PageWizardInit, self).__init__(display)
        self.pageUI = "confirm"
        self.pageTitle = N_("Setup wizard step 1/10")
    #enddef


    def show(self):
        self.items.update({
            'text' : _("Welcome to the setup wizard.\n\n"
                "This procedure is mandatory and it will help you to set up the printer.")})
        super(PageWizardInit, self).show()
    #enddef


    def contButtonRelease(self):
        self.display.state = DisplayState.WIZARD
        # check serial numbers
        if (not re.match(r"CZPX\d{4}X009X[CK]\d{5}", self.display.hw.cpuSerialNo) or
        not re.match(r"CZPX\d{4}X012X[CK01]\d{5}", self.display.hw.mcSerialNo)):
            # FIXME we don't want cut off betatesters with MC without serial number
            self.display.pages['error'].setParams(
                backFce = self.justContinue, # use as confirm
                code=Sl1Codes.SERIAL_NUMBER_IN_WRONG_FORMAT.raw_code,
                params={'a64' : self.display.hw.cpuSerialNo, 'mc' : self.display.hw.mcSerialNo}
            )
            return "error"

        #endif
        return self.justContinue() # only for confirm, join with contButtonContinue() when changed to error
    #enddef


    def justContinue(self):
        self.display.wizardData = WizardData()
        self.display.hw.powerLed("warn")
        homeStatus = 0

        #tilt home check
        pageWait = PageWait(self.display, line1 = _("Tank home check"))
        pageWait.show()
        for i in range(3):
            self.display.hw.tilt.sync_wait()    # FIXME throws exception
            homeStatus = self.display.hw.tilt.homing_status
            if homeStatus == -2:
                self.display.pages['error'].setParams(code=Sl1Codes.TILT_ENDSTOP_NOT_REACHED.raw_code)
                return "error"
            elif homeStatus == 0:
                self.display.hw.tilt.home_calibrate_wait()
                break
            #endif
        #endfor
        if homeStatus == -3:
            self.display.pages['error'].setParams(code=Sl1Codes.TILT_HOME_FAILED.raw_code)
            return "error"
        #endif

        #tilt length measure
        pageWait.showItems(line1 = _("Tank axis check"))
        self.display.hw.tilt.profile_id = TiltProfile.homingFast
        self.display.hw.tilt.move_absolute(self.display.hw.tilt.max)
        while self.display.hw.tilt.moving:
            sleep(0.25)
        #endwhile
        self.display.hw.tilt.move_absolute(512)   # go down fast before endstop
        while self.display.hw.tilt.moving:
            sleep(0.25)
        #endwhile
        self.display.hw.tilt.profile_id = TiltProfile.homingSlow    #finish measurement with slow profile (more accurate)
        self.display.hw.tilt.move_absolute(self.display.hw.tilt.min)
        while self.display.hw.tilt.moving:
            sleep(0.25)
        #endwhile
        #TODO make MC homing more accurate
        if self.display.hw.tilt.position < -defines.tiltHomingTolerance or self.display.hw.tilt.position > defines.tiltHomingTolerance:
            self.display.pages['error'].setParams(
                code=Sl1Codes.TILT_AXIS_CHECK_FAILED.raw_code,
                params={"position": self.display.hw.tilt.position}
            )
            return "error"
        #endif
        self.display.hw.tilt.profile_id = TiltProfile.homingFast
        self.display.hw.tilt.move_absolute(defines.defaultTiltHeight)
        while self.display.hw.tilt.moving:
            sleep(0.25)
        #endwhile

        #tower home check
        pageWait.showItems(line1 = _("Tower home check"))
        self.display.wizardData.towerSensitivity = self.display.hw.config.towerSensitivity
        for i in range(3):
            if not self.display.hw.towerSyncWait():
                if not self.display.doMenu("towersensitivity"):
                    return self._EXIT_()
                #endif
            #endif
        #endfor
        self.display.hw.powerLed("normal")

        #temperature check
        A64temperature = self.display.hw.getCpuTemperature()
        if A64temperature > defines.maxA64Temp:
            self.display.pages['error'].setParams(
                code=Sl1Codes.A64_OVERHEAT.raw_code,
                params={"temperature": A64temperature}
            )
            self.display.pages['error'].show()
            for i in range(10):
                self.display.hw.beepAlarm(3)
                sleep(1)
            #endfor
            shut_down(self.display.hw)
            return "error"
        #endif

        temperatures = self.display.hw.getMcTemperatures()
        for i in (self.display.hw.led_temp_idx, self.display.hw.ambient_temp_idx):
            if temperatures[i] < 0:
                self.display.pages['error'].setParams(
                    code=Sl1Codes.TEMP_SENSOR_FAILED.raw_code,
                    params={"sensor": self.display.hw.getSensorName(i)}
                )
                return "error"
            #endif
            if i == 0:
                maxTemp = defines.maxUVTemp
            else:
                maxTemp = defines.maxAmbientTemp
            #endif
            if not defines.minAmbientTemp < temperatures[i] < maxTemp:
                self.display.pages['error'].setParams(
                    code=Sl1Codes.TEMPERATURE_OUT_OF_RANGE.raw_code,
                    params={"sensor": self.display.hw.getSensorName(i), "temperature": temperatures[i]}
                )
                return "error"
            #endif
        #endfor
        self.display.wizardData.wizardTempA64 = A64temperature
        self.display.wizardData.wizardTempUvInit = temperatures[0]
        self.display.wizardData.wizardTempAmbient = temperatures[1]

        return "wizarduvled"
    #enddef

#endclass


@page
class PageWizardUvLed(PageWizardBase):
    Name = "wizarduvled"

    def __init__(self, display):
        super(PageWizardUvLed, self).__init__(display)
        self.pageUI = "confirm"
        self.pageTitle = N_("Setup wizard step 2/10")
    #enddef


    def show(self):
        self.items.update({
            'imageName' : "selftest-remove_tank.jpg",
            'text' : _("Please unscrew and remove the resin tank.")})
        super(PageWizardUvLed, self).show()
    #enddef

    def contButtonRelease(self):
        self.display.pages['confirm'].setParams(
            continueFce = self.continueCloselid,
            backFce = self.backButtonRelease,
            pageTitle = N_("Setup wizard step 3/10"),
            imageName = "selftest-remove_platform.jpg",
            text = _("Loosen the black knob and remove the platform."))
        return "confirm"
    #enddef


    def continueCloselid(self):
        self.display.pages['confirm'].setParams(
            continueFce = self.continueUvCheck,
            backFce = self.backButtonRelease,
            pageTitle = N_("Setup wizard step 4/10"),
            imageName = "close_cover_no_tank.jpg",
            text = _("Please close the orange lid."))
        return "confirm"
    #enddef


    def continueUvCheck(self):
# REMOVED
#        if not self.display.doMenu("uvfanstest"):
#            return self._EXIT_()
#        #endif

        if not self.display.doMenu("displaytest"):
            return self._EXIT_()
        #endif

        return "wizardtoweraxis"
    #enddef

#endclass


@page
class PageWizardTowerAxis(PageWizardBase):
    Name = "wizardtoweraxis"

    def __init__(self, display):
        super(PageWizardTowerAxis, self).__init__(display)
        self.pageUI = "confirm"
        self.pageTitle = N_("Setup wizard step 5/10")
    #enddef


    def show(self):
        self.items.update({
            'imageName' : "tighten_screws.jpg",
            'text' : _("Secure the resin tank with resin tank screws.\n\n"
                "Make sure the tank is empty and clean.")})
        super(PageWizardTowerAxis, self).show()
    #enddef


    def contButtonRelease(self):
        self.display.pages['confirm'].setParams(
            continueFce = self.continueTowerCheck,
            backFce = self.backButtonRelease,
            pageTitle = N_("Setup wizard step 6/10"),
            imageName = "close_cover.jpg",
            text = _("Please close the orange lid."))
        return "confirm"
    #enddef

    def continueTowerCheck(self):
        self.ensureCoverIsClosed()

        self.display.hw.powerLed("warn")
        pageWait = PageWait(self.display, line1 = _("Tower axis check"))
        pageWait.show()

        try:
            tower_axis(self.display.hw)
        except TowerAxisCheckFailed as e:
            self.display.pages['error'].setParams(
                code=Sl1Codes.TOWER_AXIS_CHECK_FAILED.raw_code,
                params=wrap_exception(e)
            )
            return "error"

        return "wizardresinsensor"
    #enddef

#endclass


@page
class PageWizardResinSensor(PageWizardBase):
    Name = "wizardresinsensor"

    def __init__(self, display):
        super(PageWizardResinSensor, self).__init__(display)
        self.pageUI = "confirm"
        self.pageTitle = N_("Setup wizard step 7/10")
    #enddef


    def show(self):
        self.items.update({
            'imageName' : "selftest-insert_platform_60deg.jpg",
            'text' : _("Insert the platform at a 60-degree angle, exactly like in the picture. The platform must hit the edges of the tank on its way down.")})
        super(PageWizardResinSensor, self).show()
    #enddef


    def contButtonRelease(self):
        self.display.pages['confirm'].setParams(
            continueFce = self.continueResinCheck,
            backFce = self.backButtonRelease,
            pageTitle = N_("Setup wizard step 8/10"),
            imageName = "close_cover.jpg",
            text = _("Please close the orange lid."))
        return "confirm"
    #enddef


    def continueResinCheck(self):
        self.ensureCoverIsClosed()

        self.display.hw.powerLed("warn")
        pageWait = PageWait(self.display,
            line1 = _("Resin sensor check"),
            line2 = _("DO NOT touch the printer"))
        pageWait.show()

# REMOVED
#        try:
#            self.display.wizardData.wizardResinVolume = resin_sensor(self.display.hw, self.logger)
#        except ResinMeasureFailed as exception:
#            self.display.pages['error'].setParams(
#                code=Sl1Codes.RESIN_SENSOR_FAILED.raw_code,
#                params=wrap_exception(exception)
#            )
#            return "error"

        self.display.hw.config.showWizard = False
        try:
            self.display.hw.config.write()
        except ConfigException as exception:
            self.logger.exception("Cannot save configuration")
            self.display.pages['error'].setParams(code=get_exception_code(exception).raw_code)
            return "error"
        #endif

        self.display.wizardData.osVersion = distro.version()
        self.display.wizardData.a64SerialNo = self.display.hw.cpuSerialNo
        self.display.wizardData.mcSerialNo = self.display.hw.mcSerialNo
        self.display.wizardData.mcFwVersion = self.display.hw.mcFwVersion
        self.display.wizardData.mcBoardRev = self.display.hw.mcBoardRevision
        self.display.wizardData.towerHeight = self.display.hw.config.towerHeight
        self.display.wizardData.tiltHeight = self.display.hw.config.tiltHeight
        self.display.wizardData.uvPwm = self.display.hw.config.uvPwm

        wizardConfig = TomlConfig(defines.wizardDataPath)
        wizardConfigFactory = TomlConfig(defines.wizardDataPathFactory)
        savedData = wizardConfig.load()
        try:
            wizardConfigFactory.data = asdict(self.display.wizardData)
            wizardConfig.data = wizardConfigFactory.data
        except AttributeError:
            self.logger.exception("wizardData is not completely filled")
            self.display.pages['error'].setParams(Sl1Codes.FAILED_TO_SERIALIZE_WIZARD_DATA.raw_code)
            return "error"
        #endtry
        wizardConfig.save_raw()
        save_wizard_history(defines.wizardDataPath)

        # store data in factory partition if in factory mode or not saved before (when user runs wizard on KIT for first time)
        if self.display.runtime_config.factory_mode or not savedData:
            if self.writeToFactory(wizardConfigFactory.save_raw):
                save_wizard_history(defines.wizardDataPathFactory)
            else:
                self.display.pages['error'].setParams(code=Sl1Codes.FAILED_TO_SAVE_WIZARD_DATA.raw_code)
                return "error"
            #endif

        self.allOff()
        return "wizardtimezone"
    #enddef

#endclass

@page
class PageWizardTimezone(PageWizardBase):
    Name = "wizardtimezone"

    def __init__(self, display):
        super(PageWizardTimezone, self).__init__(display)
        self.pageUI = "yesno"
        self.pageTitle = N_("Setup wizard step 9/10")
    #enddef


    def show(self):
        self.items.update({
            'text' : _("Do you want to setup a timezone?")})
        super(PageWizardTimezone, self).show()
    #enddef

    @staticmethod
    def yesButtonRelease():
        return "settimezone"
    #endif

    @staticmethod
    def noButtonRelease():
        return "wizardspeaker"
    #enddef

    @staticmethod
    def _BACK_():
        return "wizardspeaker"
    #enddef

#endclass


@page
class PageWizardSpeaker(PageWizardBase):
    Name = "wizardspeaker"
    SampleMusic = defines.multimediaRootPath + "/chromag_-_the_prophecy.xm"

    def __init__(self, display):
        super(PageWizardSpeaker, self).__init__(display)
        self.pageUI = "yesno"
        self.pageTitle = N_("Setup wizard step 10/10")
    #enddef


    def show(self):
        self.items.update({
            'text': _("Can you hear the music?"),
            'audio': self.SampleMusic,
        })
        super(PageWizardSpeaker, self).show()
    #enddef

    @staticmethod
    def yesButtonRelease():
        return "wizardfinish"
    #endif


    def noButtonRelease(self):
        self.display.pages['error'].setParams(code=Sl1Codes.SOUND_TEST_FAILED.raw_code)
        return "error"
    #enddef

#endclass


@page
class PageWizardFinish(PageWizardBase):
    Name = "wizardfinish"

    def __init__(self, display):
        super(PageWizardFinish, self).__init__(display)
        self.pageUI = "confirm"
        self.pageTitle = N_("Setup wizard done")
    #enddef


    def show(self):
        self.display.state = DisplayState.IDLE
        self.items.update({
            'text' : _("Selftest passed OK.\n\n"
                "Printer ready for calibration."),
            'no_back' : True })
        super(PageWizardFinish, self).show()
    #enddef

    @staticmethod
    def contButtonRelease():
        return "_EXIT_"
    #enddef

#endclass


@page
class PageWizardSkip(Page):
    Name = "wizardskip"

    def __init__(self, display):
        super(PageWizardSkip, self).__init__(display)
        self.pageUI = "yesno"
        self.pageTitle = N_("Skip wizard?")
        self.checkPowerbutton = False
    #enddef


    def show(self):
        self.items.update({
            'text' : _("Do you really want to skip the wizard?\n\n"
                "The machine may not work correctly without finishing this check.")})
        super(PageWizardSkip, self).show()
    #enddef


    def yesButtonRelease(self):
        self.display.state = DisplayState.IDLE
        self.display.hw.config.showWizard = False
        try:
            self.display.hw.config.write()
        except ConfigException as exception:
            self.logger.exception("Cannot save configuration")
            self.display.pages['error'].setParams(code=get_exception_code(exception).raw_code)
            return "error"
        #endif
        return "_EXIT_"
    #endif

    @staticmethod
    def noButtonRelease():
        return "_NOK_"
    #enddef

#endclass
