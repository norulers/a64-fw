Print project configuration
===========================

.. inheritance-diagram:: sl1fw.configs.project

.. inheritance-diagram:: sl1fw.configs.value

.. autoclass:: sl1fw.configs.project.ProjectConfig
   :members:
