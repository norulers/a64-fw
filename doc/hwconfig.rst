HWConfig
========

.. inheritance-diagram:: sl1fw.configs.hw

.. inheritance-diagram:: sl1fw.configs.value

.. autoclass:: sl1fw.configs.hw.HwConfig
   :members:

