Printer0 DBus API
=================

.. autoclass:: sl1fw.api.printer0.Printer0State
   :members:
   :undoc-members:

.. autoclass:: sl1fw.api.printer0.Printer0
   :members:

